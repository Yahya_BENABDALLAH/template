/*--------------------
Vars
--------------------*/
let progress = 50;
let startX = 0;
let active = 0;
let isDown = false;

/*--------------------
Contants
--------------------*/
const speedWheel = 0.02;
const speedDrag = -0.1;

/*--------------------
Get Z
--------------------*/
const getZindex = (array, index) => {
  
    return array.map((_, i) =>
      index === i ? array.length : array.length - Math.abs(index - i)
    )
  ;
};
/*--------------------
Items
--------------------*/
const items = document.querySelectorAll(".carousel-itema");
const cursors = document.querySelectorAll(".cursor");

const displayItems = (item, index, active) => {
  const zIndex = getZindex([...items], active)[index]; ;
  console.log(zIndex);
  item.style.setProperty("--zIndex", zIndex);
  item.style.setProperty("--active", (index - active) / items.length);
};

/*--------------------
Animate
--------------------*/
const animate = () => {
  progress = Math.max(0, Math.min(progress, 100));
  active = Math.floor((progress / 100) * (items.length - 1));

  items.forEach((item, index) => displayItems(item, index, active));
};
animate();

/*--------------------
Click on Items
--------------------*/
items.forEach((item, i) => {
  item.addEventListener("click", () => {
    progress = (i / items.length) * 100 + 10;
    animate();
  });
});

/*--------------------
Handlers
--------------------*/
const handleWheel = (e) => {
  const wheelProgress = e.deltaY * speedWheel;
  progress = progress + wheelProgress;
  animate();
};

const handleMouseMove = (e) => {
  if (e.type === "mousemove") {
    cursors.forEach(($cursor) => {
      $cursor.style.transform = `translate(${e.clientX}px, ${e.clientY}px)`;
    });
  }
  if (!isDown) return;
  const x = e.clientX || (e.touches && e.touches[0].clientX) || 0;
  const mouseProgress = (x - startX) * speedDrag;
  progress = progress + mouseProgress;
  startX = x;
  animate();
};

const handleMouseDown = (e) => {
  isDown = true;
  startX = e.clientX || (e.touches && e.touches[0].clientX) || 0;
};

const handleMouseUp = () => {
  isDown = false;
};

/*--------------------
Listeners
--------------------*/
document.addEventListener("mousewheel", handleWheel);
document.addEventListener("mousedown", handleMouseDown);
document.addEventListener("mousemove", handleMouseMove);
document.addEventListener("mouseup", handleMouseUp);
document.addEventListener("touchstart", handleMouseDown);
document.addEventListener("touchmove", handleMouseMove);
document.addEventListener("touchend", handleMouseUp);

/* -- Glow effect -- */

const blob = document.getElementById("blob");

window.onpointermove = (event) => {
  const { clientX, clientY } = event;

  blob.animate(
    {
      left: `${clientX}px`,
      top: `${clientY}px`,
    },
    { duration: 5000, fill: "forwards" }
  );
};
$(".temoignage-slick").slick({
  dots: false,
  infinite: false,
  speed: 300,
  slidesToShow: 5,
  slidesToScroll: 1,
});

$(".nav-link").click(function () {
  $(".nav-link").removeClass("active");
  $(this).addClass("active");
});

//
// ===== Scroll to Top ====
//
$(window).scroll(function () {
  if ($(this).scrollTop() >= 100) {
    // If page is scrolled more than 100px
    $("#returnTop").fadeIn(300); // Fade in the arrow
  } else {
    $("#returnTop").fadeOut(300); // Else fade out the arrow
  }
});
$("#returnTop").click(function () {
  // When arrow is clicked
  $("body,html").animate(
    {
      scrollTop: 0, // Scroll to top of body
    },
    500
  );
});

//
// ===== Date ====
//
//
function padTo2Digits(num) {
  return num.toString().padStart(2, "0");
}
function formatDate(date) {
  return [
    padTo2Digits(date.getDate()),
    padTo2Digits(date.getMonth() + 1),
    date.getFullYear(),
  ].join("");
}
//
// ===== Chargement ====
//
//
function exportSalesCsv(link, name, extenstion) {
  document.getElementById("aivam-loading").style.display = "block";
  $.ajax({
    url: link,
    method: "GET",
    xhrFields: {
      responseType: "arraybuffer",
    },
    success: function (data) {
      var a = document.createElement("a");
      const blob = new Blob([data]);
      var url = window.URL.createObjectURL(blob);
      a.href = url;
      a.download = name + "_" + formatDate(new Date()) + "." + extenstion;
      document.body.append(a);
      a.click();
      a.remove();
      window.URL.revokeObjectURL(url);
      document.getElementById("aivam-loading").style.display = "none";
    },
  });
}
$(document).on("click", "#downloadFileSales", function () {
  exportSalesCsv("export_sales_csv", "Sales-Export", "xls");
});
$(document).on("click", "#downloadVehicules", function () {
  exportSalesCsv("api/vehicules/export", "Vehicules-Export", "xlsx");
});
$(document).on("click", "#downloadUsers", function () {
  exportSalesCsv("api/users/export", "Users-Export", "xlsx");
});
