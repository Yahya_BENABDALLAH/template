$(document).ready(function () {
  const buttonShowMoreActivities = $("#button-show-more-activities");
  const buttonShowMoreOrganisation = $("#button-show-more-organisation");
  const appActivitiesCardsContainer = $("#app-activities-cards-container");
  const appOrganisationCardsContainer = $("#app-organisation-cards-container");
  // Click event Activities
  buttonShowMoreActivities.click(async function showMoreHandlers() {
    async function addContent() {
      const cards = await fetchActivities();
      const cardsElements = cards.map(createActivities);
      document.getElementById("aivam-loading").style.display = "none";
      return appActivitiesCardsContainer.append(cardsElements);
    }
    document.getElementById("aivam-loading").style.display = "block";
    setTimeout(addContent, 3000);
  });
  // Click event Organisation
  buttonShowMoreOrganisation.click(async function showMoreHandlers() {
    async function addContent() {
      const cards = await fetchOrganisation();
      const cardsElements = cards.map(createOrganisation);
      document.getElementById("aivam-loading").style.display = "none";
      return appOrganisationCardsContainer.append(cardsElements);
    }
    document.getElementById("aivam-loading").style.display = "block";
    setTimeout(addContent, 3000);
  });
  // Lists of Activities
  async function fetchActivities() {
    return [
      {
        placeholder: "Placeholder",
        title: "Titre",
        thumbnail: "Img",
        cardText:
          "This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer",
        view: "View",
        edit: "Edit",
        time: "9 mins",
      },
      {
        placeholder: "Placeholder",
        title: "Titre",
        thumbnail: "Img",
        cardText:
          "This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer",
        view: "View",
        edit: "Edit",
        time: "9 mins",
      },
      {
        placeholder: "Placeholder",
        title: "Titre",
        thumbnail: "Img",
        cardText:
          "This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer",
        view: "View",
        edit: "Edit",
        time: "9 mins",
      },
      {
        placeholder: "Placeholder",
        title: "Titre",
        thumbnail: "Img",
        cardText:
          "This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer",
        view: "View",
        edit: "Edit",
        time: "9 mins",
      },
      {
        placeholder: "Placeholder",
        title: "Titre",
        thumbnail: "Img",
        cardText:
          "This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer",
        view: "View",
        edit: "Edit",
        time: "9 mins",
      },
      {
        placeholder: "Placeholder",
        title: "Titre",
        thumbnail: "Img",
        cardText:
          "This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer",
        view: "View",
        edit: "Edit",
        time: "9 mins",
      },
    ];
  }
  // Create a new Activitie
  function createActivities(Activitie) {
    const { cardText, view, edit, time } =
      Activitie;
    return `
    <div class="col">
              <div class="card shadow-sm">
                <img
                  src="https://picsum.photos/400"
                  class="bg-primary col-12 position-relative"
                  width="100%"
                  height="275"
                  alt="IMG"
                />
                <div class="card-body">
                  <p class="card-text">${cardText}</p>
                  <div
                    class="d-flex justify-content-between align-items-center"
                  >
                    <div class="btn-group">
                      <button
                        type="button"
                        class="btn btn-sm btn-outline-secondary"
                      >
                        ${view}
                      </button>
                      <button
                        type="button"
                        class="btn btn-sm btn-outline-secondary"
                      >
                        ${edit}
                      </button>
                    </div>
                    <small class="text-muted">${time}</small>
                  </div>
                </div>
              </div>
            </div>
    `;
  }
  // Lists of Activities
  async function fetchOrganisation() {
    return [
      {
        name: "Yassir Chahid",
        fonction: "View",
      },
      {
        name: "Yassir Chahid",
        fonction: "View",
      },
      {
        name: "Yassir Chahid",
        fonction: "View",
      },
      {
        name: "Yassir Chahid",
        fonction: "View",
      },
      {
        name: "Yassir Chahid",
        fonction: "View",
      },
    ];
  }
  // Create a new Activitie
  function createOrganisation(Organisation) {
    const { name, fonction } = Organisation;
    return `
            <div class="col mt-5 mb-4">
              <div class="card shadow-sm">
              <img
                  src="https://picsum.photos/400"
                  class=" shadow-sm rounded-3 img-organisation"
                  height="240"
                  alt="IMG"
                />
                <div class="card-body profil-organisation textdetail">
                  <h6 class="card-text">
                    ${name}
                  </h6>
                  <h6 class="card-text">
                    ${fonction}
                  </h6>
                </div>
              </div>
            </div>
    `;
  }
});
